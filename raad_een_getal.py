"""
Python3.8.3rc1
Naam programma: raadspel.py
Author: Ernest Lassman
laatst gewijzigs op 18/06/2020
De gebruiker moet een willekerig getal raden dat door de computer 
'in gedachten genomen' is.
"""

# Getallen raden
import random  # Random-library gebruiken

# Initialiseren
max_pogingen = 10  # Maximum aantal pogingen om het getal te raden
bovengrens = 100  # Bovengrens voor het te raden getal

# Kies een willekeurig geheel getal tussen 1 en bovengrens
antwoord = random.randint(1, bovengrens)  # De computer kiest een getal

poging = 1  # De eerste poging om te raden
gebruikers_getal = bovengrens   # De gebruikers_getal is het getal dat de gebruiker zal ingeven

# Een gebruiker krijgt niet meer dan max_pogingen om het getal te raden en heeft het getal nog niet goed
while poging < max_pogingen and gebruikers_getal != antwoord:
    gebruikers_getal = int(input("Raad een getal onder de 100: "))  # Het programma begint met deze boodschap
    if gebruikers_getal > antwoord:     # De gebruiker heeft het getal niet goed
        print("Lager!")                 # De gebruiker wordt hier gevraagd om lager te raden
        poging += 1                     # Het aantal pogingen neemt toe
    elif gebruikers_getal < antwoord:   # De gebruiker heeft het getal niet goed
        print("Hoger!")                 # De gebruiker wordt gevraagd om hoger te raden
        poging += 1                     # Het aantal pogingen neemt toe

# Het maximum aantal pogingen om het getal te raden is bereikt en de gebruiker heeft het getal NIET geraden
if poging == max_pogingen and gebruikers_getal != antwoord:
    print("Helaas, u heeft het getal niet geraden.")  # Het programma weergeeft dit bericht

# Het maximum aantal pogingen om het getal te raden is NIET bereikt en de gebruiker heeft getal geraden 
elif poging < max_pogingen and gebruikers_getal == antwoord:
    # de gebruiker heeft kunnen raden na 1 poging
    if poging == 1:
        print("Goed geraden!\nEn dat in {0} poging.".format(poging))    # Het programma weergeeft dit bericht
    # de gebruiker heeft kunnen raden na meer dan 1 poging
    else:
        print("Goed geraden!\nEn dat in {0} pogingen.".format(poging))  # Het programma weergeeft dit bericht

